# Scrabbleable

Make Scrabble wordlists from text files.

## Install

## Use

## Development

Initial setup:

    virtualenv .venv
    source .venv/bin/activate
    python3 -m pip install --upgrade pip
    python3 -m pip install -r requirements.txt

Testing:

    make test

Linting:

    make lint

Packaging:

    make package
