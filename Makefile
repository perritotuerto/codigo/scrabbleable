.PHONY: pytest lint test tests package

PANDOC := $(shell type pandoc 2>/dev/null)

tests: test

test:
		pytest

lint:
ifdef PANDOC
		pandoc --markdown-headings=atx --reference-links --reference-location=document --columns=79 -o README.md README.md
endif
		python3 -m black --line-length 79 --quiet scrabbleable/ tests/ 
		python3 -m flake8 --max-line-length 79 scrabbleable/ tests/ 

package:
		python3 -m pip install --upgrade build pip
		python3 -m build

