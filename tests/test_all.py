import pytest
from pathlib import Path
from scrabbleable.cli import main
from scrabbleable.wordlist import WordList

ASSETS = Path(__file__).parent.parent / "tests" / "assets"
TXT_RAW = ASSETS / "corpes_10000_formas_ortograficas.txt"
TXT_OUT = ASSETS / "wordlist_corpes.txt"


def assert_out(capsys, msg, *args):
    """Asserts that message is in STDOUT or STDERR"""
    args = tuple(map(lambda a: str(a), args))
    if isinstance(msg, str):
        try:
            main(args)
        except SystemExit:
            pass
        res = capsys.readouterr().out + capsys.readouterr().err
    elif "Error" in str(msg):
        with pytest.raises(msg) as info:
            main(args)
        res = str(info.type) + str(info.value)
    else:
        with pytest.warns(UserWarning) as info:
            main(args)
        res = str(info.expected_warning) + str([str(e.message) for e in info])
    assert str(msg) in res


def test_cli(capsys):
    cmds = [
        (OSError, "fail"),
        (IOError, ASSETS),
        (ValueError, "-l", "fail", TXT_RAW),
        (UserWarning, "-i", 99999, TXT_RAW),
        (UserWarning, "-l", "es", "-o", TXT_OUT, TXT_RAW),
        ("usage", "-h"),
        ("ABAJO", "-s", TXT_RAW),
        ("DE", "-s", "-i", 1, TXT_RAW),
        ("Y", "-s", "-i", 1, "-w", 1, 1, TXT_RAW),
    ]
    [assert_out(capsys, args[0], *args[1:]) for args in cmds]


def test_class_attrs_defaults():
    wlist = WordList([TXT_RAW])
    assert wlist.paths == [TXT_RAW]
    assert wlist.lang == "en"
    assert wlist.max == 0
    assert wlist.size == (2, 15)


def test_class_attrs_custom():
    wlist = WordList([TXT_RAW])
    wlist.paths = [TXT_OUT]
    wlist.lang = "es"
    wlist.max = 1
    wlist.size = (1, 1)
    wlist2 = WordList([TXT_OUT], lang="es", max_items=1, word_size=(1, 1))
    assert wlist.paths == [TXT_OUT]
    assert wlist.lang == "es"
    assert wlist.max == 1
    assert wlist.size == (1, 1)
    assert wlist.paths == wlist2.paths
    assert wlist.lang == wlist2.lang
    assert wlist.max == wlist2.max
    assert wlist.size == wlist2.size


def test_class_results_default():
    wlist = WordList([TXT_RAW])
    wlist.make()
    assert len(wlist.words) == 8786


def test_class_results_lang():
    wlist = WordList([TXT_RAW], lang="es")
    wlist.make()
    assert len(wlist.words) == 8756
    assert WordList.TILES["es"] == wlist.tiles
    assert WordList.TILES["en"] != wlist.tiles
    assert wlist.max == 0


def test_class_results_max():
    wlist = WordList([TXT_RAW], max_items=1)
    wlist.make()
    assert len(wlist.words) == 1


def test_class_results_size():
    wlist = WordList([TXT_RAW], word_size=(1, 1))
    wlist.make()
    assert len(wlist.words) == 23
