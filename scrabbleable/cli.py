import argparse
from pathlib import Path
from scrabbleable.wordlist import WordList


def parse(args=None):
    """Parses all arguments

    :param args: Test arguments; if None, then uses CLI args; None by default
    :type args: tuple | None
    """
    parser = argparse.ArgumentParser(
        prog="scrabbleable",
        description="Make Scrabble wordlists from text files",
    )
    parser.add_argument(
        "paths",
        metavar="PATHS",
        type=Path,
        nargs="+",
        help="one or more paths",
    )
    parser.add_argument(
        "-s",
        "--stdout",
        action="store_true",
        help="print wordlist",
    )
    parser.add_argument(
        "-o",
        "--output",
        metavar="OUTPUT",
        type=Path,
        help="save wordlist in a file",
    )
    parser.add_argument(
        "-l",
        "--lang",
        metavar="LANGUAGE",
        default="en",
        help="indicate wordlist lang; 'en' by default",
    )
    parser.add_argument(
        "-i",
        "--max-items",
        metavar="INTEGER",
        type=int,
        default=0,
        help="indicate max length for wordlist; no limit by default",
    )
    parser.add_argument(
        "-w",
        "--word-length",
        metavar=("MIN", "MAX"),
        type=int,
        default=(2, 15),
        nargs=2,
        help="indicate min and max length for each word; (2, 15) by default",
    )

    return parser.parse_args(args)


def main(args=None):
    """Inits Scrabbleable class according to CLI subcommands

    :param args: Test arguments; if None, then uses CLI args; None by default
    :type args: tuple | None
    """
    args = parse(args)
    wlist = WordList(args.paths, args.lang, args.max_items, args.word_length)
    wlist.make()
    if args.stdout:
        wlist.show()
    if args.output:
        wlist.write(args.output)
